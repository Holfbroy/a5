<?php

namespace GRTracker\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Redis;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function showSinglePage($page_id)
    {

        return view('page-template', [
            'pages' => $this->helperPageProvider(),
            'page_id' => $page_id
        ]);
    }

    private function helperPageProvider()
    {

        $pagesYoutubeHashes = [
            1 => ['page_id'=> 1],
            2 => ['page_id'=> 2],
            3 => ['page_id'=> 3],
        ];


        return $pagesYoutubeHashes;
    }

}
