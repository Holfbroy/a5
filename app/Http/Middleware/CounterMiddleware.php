<?php

namespace GRTracker\Http\Middleware;

use Redis;
use Request;
use Config;
use Closure;


class CounterMiddleware
{
    const VISIT_PREFIX = 'visit';
    const BASE = "stat:page:"; /*The sorted set of site stats basic prefix */
    const SUMMARY = "summary";
    const UNDEFINED = "undefined";
    const BROWSER = ":browser:";
    const OS = ":os:";
    const GEO = ":geo:";
    const REF = ":ref:";
    const IP = ":ip";
    const HIT = ":hit";
    const SESSION = ":session";
    private $uhash; /* A visit unique hash value */
    private $agent;
    private $user_ip;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle ( $request , Closure $next )
    {
        $this->agent = get_browser();
        $this->user_ip = Request::ip();

        $this->insertBrowser($request->route('page_id'));
        $this->insertOs($request->route('page_id'));
        $this->insertGeo($request->route('page_id'));
        $this->insertRef($request->route('page_id'));
        $this->insertSummaryReportData();

        return $next( $request );
    }

    private function getAssuredUniqHash($key)
    {
        $asHash = uniqid(rand(1,10),true).time();

        if(Redis::sismember($key, $asHash)) {
            $this->getAssuredUniqHash($key);
        } else {
            return $asHash;
        }
    }

    private function insertSummaryReportData()
    {
        $this->insertBrowser(self::SUMMARY);
        $this->insertOs(self::SUMMARY);
        $this->insertGeo(self::SUMMARY);
        $this->insertRef(self::SUMMARY);

    }

    private function insertBrowser($page_id)
    {
        $browserSet = self::BASE.$page_id.self::BROWSER.$this->agent->browser;

        Redis::sadd($browserSet.self::IP, $this->user_ip);
        Redis::sadd($browserSet.self::HIT, $this->getAssuredUniqHash($browserSet.self::HIT));
        Redis::sadd($browserSet.self::SESSION, $this->helperGetCurrentCookieId());
    }

    private function insertOS($page_id)
    {
        $OsSet = self::BASE.$page_id.self::OS.$this->agent->platform;

        Redis::sadd($OsSet.self::IP, $this->user_ip);
        Redis::sadd($OsSet.self::HIT, $this->getAssuredUniqHash($OsSet.self::HIT));
        Redis::sadd($OsSet.self::SESSION, $this->helperGetCurrentCookieId());

    }

    private function insertRef($page_id)
    {
        if(empty(Request::server('HTTP_REFERER'))) {
            $parsedUrl = [];
            $parsedUrl['host'] = self::UNDEFINED;
        } else {
            $parsedUrl= parse_url(Request::server('HTTP_REFERER'));
        }

        $RefSet = self::BASE.$page_id.self::REF.$parsedUrl['host'];

        Redis::sadd($RefSet.self::IP, $this->user_ip);
        Redis::sadd($RefSet.self::HIT, $this->getAssuredUniqHash($RefSet.self::HIT));
        Redis::sadd($RefSet.self::SESSION, $this->helperGetCurrentCookieId());

    }

    private function insertGeo($page_id)
    {
        /*отключен для тестовой задачи - так как кол-во запросов бесплатной подписки совсем маленькое */
        /*$GeoSet = self::BASE.$page_id.self::GEO.$this->helperGetGeoData();*/

        $GeoSet = self::BASE.$page_id.self::GEO.self::UNDEFINED;/* заглушка - будет инсертить "undefined" в геоданные в редис */

        Redis::sadd($GeoSet.self::IP, $this->user_ip);
        Redis::sadd($GeoSet.self::HIT, $this->getAssuredUniqHash($GeoSet.self::HIT));
        Redis::sadd($GeoSet.self::SESSION, $this->helperGetCurrentCookieId());
    }

    private function helperGetCurrentCookieId()
    {
        $currentCookiesData = Request::cookie();

        if(empty($currentCookiesData)) {
            $currentSessionHash = self::UNDEFINED;
        } else {
            $currentSessionHash = $currentCookiesData['laravel_session'];
        }

        return $currentSessionHash;
    }

    private function helperGetGeoData()
    {
        /*! на проде без платной подписки сервиса геотиггинга отвалится через несколько минут - так как много запросов
         *  это реалистичный вариант хранения данных в виде гео сигнатуры "RU Pyatigorsk KMV"
         * полученные через сервис гео-локации по айпи адресу посетителя.
         * По понятным причинам делать запрос при каждом хите к сервису очень дорогое удовольствие,но
         * как правило для небольших проектов это нормальная практика - для больших можно к примеру
         * хранить локально данные для гео-таггинга.
         * Сам же Redis использует геоданные as specified by EPSG:900913 / EPSG:3785 / OSGEO:41001
         * are the following:
         *   Valid longitudes are from -180 to 180 degrees.
         *   Valid latitudes are from -85.05112878 to 85.05112878 degrees.
         * Далее эти данные можно использовать для команда раздела Geo
         *
        */

        $ipValue    = trim(Request::ip());
        $result     = file_get_contents('http://api.ipinfodb.com/v3/ip-city/?key=' . Config::get('app.ipinfodb_key') . '&ip=' . $ipValue . '&format=json');
        $clientData = json_decode($result, true);
        $geotag = $clientData["countryCode"] . '|' . $clientData["cityName"] . '|' . $clientData["regionName"];

        if($geotag == '-|-|-') {
            /*The empty response signature - probably localhost request */
            $geotag = 'UNKNOWN|UNKNOWN|UNKNOWN';
        };

        return $geotag;
    }

}
