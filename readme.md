# a5 test task

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

На базе laravel 5.2 + twig bridge в качестве view  реализовать следующую систему:

Cоздаем простейший  сайт делая верстку на основе http://getbootstrap.com/
Содержимое: любой тестовый текст + картинки. Достаточно 2 страниц`. 

2) Где-то на странице разместить ссылку включить версию для слабовидящих.

3) Реализовать версию для слабовидящих. Нам нравится реализация вот этих ребят
http://finevision.ru/?hostname=www.a5.ru&path=/

Можно подсматривать и сделать 1 в 1 ( т.е. Технически работает как замена шрифтов и цветов исходного сайта посредством js )

Сверху страницы бар для управления версией.




